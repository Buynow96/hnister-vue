import menuApi from '../../api/menu'
import menuEnum from '../../enums/menuEnum'
import Vue from 'vue'


const state = {
    headMenuTree:{}
}

const getters = {
    headMenuTree:(state)=>{
        return state.headMenuTree;
    }
}

//只进行同步功能 异步操作在action中进行
const mutations = {
    headMenuTree:(state,headMenuTree)=>{
        Vue.set(state,menuEnum.headMenuTree,headMenuTree)
    }
}

//异步操作
const actions = {
    headMenuTree:(context)=>{
        menuApi.getHeadMenuTree()
            .then(data => {
                if(data != null){
                    context.commit(menuEnum.headMenuTree,data)
                }
            })
    }
}

export default{
    state,
    getters,
    mutations,
    actions
}