import Vue from 'vue'
import Router from 'vue-router'

import app from '@/App'

//views
import Index from '@/views/Index'
import indexMain from '@/views/index/Main'
import Account from '@/views/index/Account'
import NewsTheme from '@/views/index/news/NewsModule'
import NewsContent from '@/views/index/news/NewsContent'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'indexMain',
            redirect:'/index/main',
            component: indexMain
        },
        {
            path:'/index',
            name:'Index',
            component:Index,
            children:[
                {
                    //首页主页
                    path: '/index/main',
                    name: 'indexMain',
                    component: indexMain
                },
                {
                    //登录页面
                    path:'/index/account',
                    component:Account
                },
                {
                    path:'/index/news/module/:moduleId',
                    props:true,
                    component:NewsTheme
                },
                {
                    path:'/index/news/:newsId',
                    props:true,
                    component:NewsContent
                },
            ]
        },

    ]
})
