import {cloudServiceName} from '../utils/global'
import request from '../utils/request'

export default{
    getHeadMenuTree:function(){
        return request({
            method:'get',
            url:cloudServiceName.settingService + '/rest/pb/menuTree/groupId/1'
        })
    }
}