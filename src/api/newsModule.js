import {cloudServiceName} from '../utils/global'
import request from '../utils/request'

export default{
    findById:function(mid){
        return request({
            method:'get',
            url:cloudServiceName.newsService + '/rest/pb/newsModule/' + mid,
        })
    }
}