import axios from 'axios'
import {address} from './global'

// 创建axios实例
const service = axios.create({
    // baseURL: process.env.BASE_API, // api的base_url
    baseURL: address.zuul,
    timeout: 15000 // 请求超时时间
})

const responseFun = function (response) {
    return response.data;
}

// respone拦截器
service.interceptors.response.use(
    responseFun,
    error => {
        switch (error.response.status){
            case 50008:
                Message({
                    type: 'error',
                    message: '您的权限不满足此操作',
                    duration: 5 * 1000
                })
                return Promise.reject(error)
        }

        console.log('err' + error)// for debug
        Message({
            message: error.message,
            type: 'error',
            duration: 5 * 1000
        })
        return Promise.reject(error)
    }
)


export default service

